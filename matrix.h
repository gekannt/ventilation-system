#ifndef MATRIX_H
#define MATRIX_H

#include "inclusion.h"
#include <mpi/mpi.h>
#include "a_parrallel.h"

template <class T>
class Matrix
{
 public:
    Matrix<T>();
    Matrix<T>(int row, int column);
    Matrix<T>(T *array, int row, int column);
    Matrix<T>(const Matrix<T> &matrix);
    Matrix<T> &operator= (const Matrix<T> &source);

    ~Matrix();

    Matrix<T> operator+(const Matrix<T> &second_operand);
    Matrix<T> operator-(const Matrix<T> &second_operand);
    Matrix<T> operator*(const Matrix<T> &second_operand);


    void set_to_value (const double &initial_value);
    void show(const char *sting_info) const;
    void transpose();
    void swap_rows(int first, int second);
    void make_indentity_matrix();
    int get_size_columns() const { return size_columns_; }
    int get_size_rows( ) const  { return size_rows_; }
    T *get_pointer_to_data() const { return data_; } //very bad  design decision


 // GAUSS_JORDAN
    int remove_possible_zero_from_row_in_the_first_column(int number_of_row, int offset, Matrix <T> &identity_matrice);
    void make_row_equal_to_one(int number_of_row, int offset, Matrix<T> &identity_matrice);
    void make_lower_elements_equal_to_zero(int number_of_row_source_of_action, int number_of_row_to_do_action,
                                           int offset, Matrix<T> &identity_matrice);
    Matrix<T> gauss_jordan_inverse_matrice();
    void make_lower_elements_equal_to_zero_back(int number_of_row_source_of_action, int number_of_row_to_do_action,
                                                int offset, Matrix<T> &identity_matrice);
 // END  GAUSS_JORDAN

    friend Matrix<T> operator*(const Matrix<T> &second_operand,const T number) {
        Matrix <T> copy(second_operand);
        for(int  i=0; i< copy.size_columns_ * copy.size_rows_; i++)
            copy.data_[i] = copy.data_[i]  * number;
        return copy;
    }

    friend Matrix<T> operator*(const T number, const Matrix<T> &second_operand) {
        Matrix <T> copy(second_operand);
        for(int  i=0; i< copy.size_columns_ * copy.size_rows_; i++)
            copy.data_[i] = copy.data_[i]  * number;
        return copy;
    }

    friend void calculate_matrices( int rank, int number_processes);

public:
private:
    int size_rows_,size_columns_;
    T *data_;

   int rank_, number_processes_;
};

template <class T>
void Matrix<T>::transpose() {
    int column=0,row=0;

    if( this->size_columns_ == 1 || this->size_rows_ == 1) {
       swap(size_columns_,size_rows_);
       return;
    }

    Matrix <T> copy(*this);

    while (row < size_columns_)  {
        column=0;
        while (column < size_rows_) {
            data_[row*size_rows_+column] = copy.data_[column*size_columns_+row];
            column++;
        }
      row++;
    }

  swap(this->size_columns_,this->size_rows_);
}


template<class T>
Matrix<T> Matrix<T>::operator *(const Matrix<T> &second_operand) {

const int MASTER_TO_SLAVE_TAG = 20; //tag for messages sent from master to slaves
const int SLAVE_TO_MASTER_TAG =4; //tag for messages sent from slaves to master

 int rank=0; //process rank
 int number_processes=0;

 assert(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == MPI_SUCCESS); //get the rank
 assert ( MPI_Comm_size(MPI_COMM_WORLD, &number_processes) == MPI_SUCCESS ); //get number of processes

//   Matrix<T> result(this->size_rows_,second_operand.size_columns_);
 static int work_counter = 0;


   double start_time=0,end_time=0; //hold start and end time
   int low_bound=0, upper_bound=0, portion=0; //low bound of the number of rows of [A] allocated to a slave
    //upper bound of the number of rows of [A] allocated to a slave
    //portion of the number of rows of [A] allocated to a slave
   MPI_Status status; // store status of a MPI_Recv
   MPI_Request request; //capture request of a MPI_Isend

//   wake_up_processes(rank,number_processes);

//       start_time = MPI_Wtime();
    portion = (this->size_rows_ / (number_processes - 1)); // calculate portion without master

//       if ( work_counter == 1) {
//           MPI_Finalize();
//           exit(0);
//       }


    if (portion !=0) {

      printf("FIRST!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n\n");
      wake_up_processes(rank,number_processes);




      for (int i = 1; i < number_processes; i++)  {
          //for each slave other than the master
           low_bound = (i - 1) * portion;
           if (((i + 1) == number_processes) && ((this->size_rows_ % (number_processes - 1)) != 0)) {
               //if rows of [A] cannot be equally divided among slaves
               upper_bound = this->size_rows_; //last slave gets all the remaining rows
//               printf("CANNOT BE DIVIDED\n");
           }
           else {
               upper_bound = low_bound + portion; //rows of [A] are equally divisable among slaves
           }
           //send the low bound first without blocking, to the intended slave
           MPI_Isend(&low_bound, 1, MPI_INT, i, MASTER_TO_SLAVE_TAG, MPI_COMM_WORLD, &request);

           //next send the upper bound without blocking, to the intended slave
           MPI_Isend(&upper_bound, 1, MPI_INT, i, MASTER_TO_SLAVE_TAG + 1, MPI_COMM_WORLD, &request);

           // send number of elements being transferred
//           int number_of_elements_to_send = (upper_bound - low_bound) * this->size_columns_;
           int size_columns = this->size_columns_;
           MPI_Isend(&size_columns, 1, MPI_INT, i, MASTER_TO_SLAVE_TAG + 2, MPI_COMM_WORLD, &request);

           //finally send the allocated row portion of [A] without blocking, to the intended slave

           // all templates
           MPI_Isend( this->data_ + low_bound*this->size_columns_, (upper_bound - low_bound) * this->size_columns_, MPI_DOUBLE, i,
                           MASTER_TO_SLAVE_TAG + 3, MPI_COMM_WORLD, &request);


           MPI_Isend(const_cast<int *> (&second_operand.size_rows_), 1, MPI_INT, i, MASTER_TO_SLAVE_TAG + 4, MPI_COMM_WORLD, &request);
           MPI_Isend(const_cast<int *> (&second_operand.size_columns_), 1, MPI_INT, i, MASTER_TO_SLAVE_TAG + 5, MPI_COMM_WORLD, &request);

       }
    }
    else  { // if number of processes is bigger than number of rows in matrice
        printf("SECOND!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n\n");
        wake_up_processes(rank,0,this->size_rows_+1);

        portion = 1;
        for (int i = 1; i < this->size_rows_+1; i++)  {
            //for each slave other than the master
             low_bound = (i - 1) * portion;
             upper_bound = low_bound + portion; //rows of [A] are equally divisable among slaves

             //send the low bound first without blocking, to the intended slave
             MPI_Isend(&low_bound, 1, MPI_INT, i, MASTER_TO_SLAVE_TAG, MPI_COMM_WORLD, &request);

             //next send the upper bound without blocking, to the intended slave
             MPI_Isend(&upper_bound, 1, MPI_INT, i, MASTER_TO_SLAVE_TAG + 1, MPI_COMM_WORLD, &request);

             int size_columns = this->size_columns_;
             MPI_Isend(&size_columns, 1, MPI_INT, i, MASTER_TO_SLAVE_TAG + 2, MPI_COMM_WORLD, &request);

             //finally send the allocated row portion of [A] without blocking, to the intended slave

             // all templates
             MPI_Isend( this->data_ + low_bound*this->size_columns_, (upper_bound - low_bound) * this->size_columns_, MPI_DOUBLE, i,
                             MASTER_TO_SLAVE_TAG + 3, MPI_COMM_WORLD, &request);

             MPI_Isend(const_cast<int *> (&second_operand.size_rows_), 1, MPI_INT, i, MASTER_TO_SLAVE_TAG + 4, MPI_COMM_WORLD, &request);
             MPI_Isend(const_cast<int *> (&second_operand.size_columns_), 1, MPI_INT, i, MASTER_TO_SLAVE_TAG + 5, MPI_COMM_WORLD, &request);
//             MPI_Isend(const_cast<int *> (&second_operand.size_columns_), 1, MPI_INT, i, MASTER_TO_SLAVE_TAG + 4, MPI_COMM_WORLD, &request);
//             MPI_Isend(const_cast<int *> (&second_operand.size_rows_), 1, MPI_INT, i, MASTER_TO_SLAVE_TAG + 5, MPI_COMM_WORLD, &request);

         }

    }

//    if ( work_counter == 1) {
//        MPI_Finalize();
//        exit(0);
//    }
//    MPI_Wait(&request,&status);

//    second_operand.show("SECOND OPERAND");
    Matrix<T> copy= second_operand;
    copy.transpose();
    copy.show("BEFORE SECOND");

    const int ROOT_PROCESS_ID = 0;
//    assert(  MPI_Bcast(second_operand.get_pointer_to_data(),
//              second_operand.size_rows_ * second_operand.size_columns_, MPI_DOUBLE, ROOT_PROCESS_ID, MPI_COMM_WORLD) == MPI_SUCCESS );


//    if ( work_counter == 1) {
//        MPI_Finalize();
//        exit(0);
//    }

    cerr<<"BCAST  ROOOT   >>>>>>>>>>>>>>>>>"<<work_counter<<"\n";
   assert(  MPI_Bcast(copy.get_pointer_to_data(),
              copy.size_rows_ * copy.size_columns_, MPI_DOUBLE, ROOT_PROCESS_ID, MPI_COMM_WORLD) == MPI_SUCCESS );


   const int RECV_RESULT = 36;

   T *array = new T[this->size_rows_ * second_operand.size_columns_];



   for(int i = 1; i < this->size_rows_+1; i++ ) {
       MPI_Recv(array+(i-1)* second_operand.size_columns_,
                this->size_rows_*second_operand.size_columns_,
                MPI_DOUBLE, i, RECV_RESULT+i-1, MPI_COMM_WORLD, &status);
    }


//    cout<<this->size_rows_*second_operand.size_columns_<<"\n";

    Matrix<T> holder(array, this->size_rows_, second_operand.size_columns_);
    holder.show("BACK");


    delete []array;

//    finalise_up_processes(rank, number_processes);
    work_counter ++;

   return  holder;
}



template <class T>
void Matrix<T>::set_to_value (const double &initial_value) {

    int number_elements= this->size_columns_ * this->size_rows_;

    for (int row = 0; row <number_elements ; ++row) {
        this->data_[row]=initial_value;
    }
}



template <class T>
Matrix<T> concatenate_vertically (const  Matrix<T> &first, const Matrix<T> &second) {

    if (first.get_size_rows() != second.get_size_rows()) {
        first.show("first");
        second.show("second");
        ERROR_EXIT("impossible to concatenate such matrices");
    }

    Matrix <T>result(first.get_size_rows(),first.get_size_columns()+second.get_size_columns());
    T *result_data = result.get_pointer_to_data(), *first_data =first.get_pointer_to_data(),
                             *second_data =second.get_pointer_to_data();// very bad approach

// first matrix
    for( int row=0; row< first.get_size_rows(); row++) {
      for ( int column=0; column < first.get_size_columns(); column++) {
          result_data[row*result.get_size_columns() + column] = first_data[row*first.get_size_columns() + column];
      }
    }
// second matrix
    int counter_second_columns;
    for(int row=0; row < second.get_size_rows(); row++) {
        counter_second_columns=0;
        for( int column = first.get_size_columns(); column< first.get_size_columns()+second.get_size_columns(); column++, counter_second_columns++) {
            result_data[row* result.get_size_columns() + column] =
                    second_data[row*second.get_size_columns() +counter_second_columns ];

        }
    }
  return result;
}


template <class T>
void Matrix<T>::show( const char *s) const {
  if ( data_ !=NULL) {
    printf("%s \n", s);
//      cout<<s<<"\n";
    for (int i=0; i<size_rows_; i++) {
       for ( int j=0;j<size_columns_;j++)
        cout<<data_[j + size_columns_*i]<<" ";
        cout<<"\n";
    }
  } else  error_warning("data_ is equal to zero, they cannot be shown");
}

#include "matrix_basic.h"
#include "matrix_inversing.h"

#endif // MATRIX_H
