#ifndef MATRIX_TPP_H
#define MATRIX_TPP_H

template <class T>
Matrix<T>::Matrix():size_columns_(0), size_rows_(0), data_(NULL) { }

template <class T>
Matrix<T>::Matrix(T *array, int row, int column): size_rows_(row), size_columns_(column) {
  data_ = new(nothrow) T [size_rows_ * size_columns_];

  if ( data_  == NULL)
    error_fatal("delete operator can't allocate memory");
  memcpy(data_, array, size_columns_ * size_rows_* sizeof(T));
}


template <class T>
Matrix<T>::Matrix(const Matrix<T> &Matrix ) : size_columns_ (Matrix.size_columns_), size_rows_ (Matrix.size_rows_) {
    data_ = new T[Matrix.size_columns_ * Matrix.size_rows_ ];
    memcpy(data_, Matrix.data_, Matrix.size_columns_ * Matrix.size_rows_ * sizeof(T) );
}


template <class T>
Matrix<T>::Matrix(int row, int column): size_rows_(row), size_columns_(column),
                  data_(new(nothrow) T [size_columns_ * size_rows_]())   {
    if ( data_ == NULL)   error_fatal("failed creation of indentity matrix, not enough memory");
}

template <class T>
void Matrix<T>::make_indentity_matrix() {

   int position =0;
   for(int  i=0; i< size_rows_; i++) {
         data_[i*size_columns_ + position ] = 1;
         position++;
   }
}

//DANGER ~!~~~~~~~~~~~~!!!!!!
//template <class T>
//void Matrix<T>::transpose() {
//    int column=0,row=0;

//    if( this->size_columns_ == 1 || this->size_rows_ == 1) {
//       swap(size_columns_,size_rows_);
//       return;
//    }

//    Matrix <T> copy(*this);

//    while (row < size_rows_)  {
//        column=0;
//        while (column< size_columns_) {
////            double a=copy.data_[column*size_columns_+row];
////            double b=data_[row*size_columns_+column];
//            data_[row*size_columns_+column] = copy.data_[column*size_columns_+row];
//            column++;
//        }
//      row++;
//    }

//  swap(size_columns_,size_rows_);
//}





template <class T>
Matrix<T> Matrix<T>::operator+(const Matrix<T> &second_operand) {

    Matrix <T> copy(*this);
    int  rank=0;
    assert(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == MPI_SUCCESS); //get the rank
    if ( rank!=0)
        return copy;


               if ( size_columns_ != second_operand.size_columns_  &&
                    size_rows_ != second_operand.size_rows_ ) {

                   show("matrice 1");
                   second_operand.show("matrice 2");
                    error_fatal("addition of matrices is impossible, sizes are different");
               }


               for ( int i=0;i<copy.size_rows_*copy.size_columns_;i++)
                 copy.data_[i]+=second_operand.data_[i];
       return copy;
}


template <class T>
Matrix<T> Matrix<T>::operator-(const Matrix<T> &second_operand) {

    Matrix <T> copy(*this);
    int rank=0;
    assert(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == MPI_SUCCESS); //get the rank
    if ( rank!=0)
        return copy;

               if ( size_columns_ != second_operand.size_columns_  &&
                    size_rows_ != second_operand.size_rows_ ) {
                    show("matrice 1");
                    second_operand.show("matrice 2");
                    error_fatal("addition of matrices is impossible, sizes are different");
               }

               for ( int i=0;i<copy.size_rows_*copy.size_columns_;i++)
                 copy.data_[i]-=second_operand.data_[i];
       return copy;
}



template <class T>
Matrix<T>::~Matrix() {
    if (data_ != NULL) {
        delete []data_;
        data_ = NULL;
    }
}


template <class T>
Matrix<T> &Matrix<T>::operator =(const Matrix<T> &source)  {


    if ( &source !=  this) {
        if(data_!= NULL)  delete [] data_;

        data_ = new (nothrow) T [ source.size_columns_ * source.size_rows_ ]();

        if ( data_  == NULL) error_fatal("delete operator can't allocate memory");

        memcpy(data_,  source.data_, source.size_columns_ * source.size_rows_* sizeof(T));
        size_rows_ = source.size_rows_;
        size_columns_ = source.size_columns_;
    }

  return *this; // for operators chaining like a=b=c=d
}


#endif // MATRIX_TPP_H
