#ifndef INCLUSION_H
#define INCLUSION_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <vector>
#include <set>
#include <algorithm>
#include <cassert>
#include <unistd.h>
#include <mpi/mpi.h>

using namespace std;


#define ERROR_EXIT(line)\
{  \
    cout<<__FILE__<<"  " << __LINE__ << "  "<<line<<endl; \
    exit(0);   \
}

#define ERROR 1

#define  cout_message(cause)\
{ cout<<__FILE__<<"   "<<__LINE__<<"  "<<cause<<"\n";  \
}

#define cerr_message(cause) \
{ cerr<<__FILE__<<"   "<<__LINE__<<"  "<<cause<<"\n";  \
}

#define error_warning(string) {  \
    cerr<<"warning error "<<__FILE__<<" "<<__LINE__<<" "<<string<<"\n"; \
}

#define error_fatal(string) {  \
    cerr<<"fatal error "<<__FILE__<<" "<<__LINE__<<" "<<string<<"\n"; \
    exit(-1); \
}

#endif // INCLUSION_H
