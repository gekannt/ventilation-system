#ifndef A_PARRALLEL_H
#define A_PARRALLEL_H

#include "matrix.h"

const int FINISH_VALUE = 4, WAKE_UP = 5;

template <class T>
class Matrix;

template <class T>
void calculate_matrices(int rank, int number_processes);

void stall_non_root_processes(int rank, int number_of_processes) {

  const int TAG_WAIT = 14;
  int  waiting_value = 0;

  if ( rank != 0) {
   while ( true ) {

      MPI_Status status;
      cout<<"non-root are stalled\n";

      MPI_Recv(&waiting_value, 1, MPI_INT, 0, TAG_WAIT, MPI_COMM_WORLD, &status); // go into waiting state

      if ( waiting_value == TAG_WAIT) {
          printf("processor is out %d  \n", rank);
          MPI_Finalize();  // additional processes are not necessary anymore
          exit(0);
      }

//      printf("waiting value %d \n", waiting_value);
      printf("calculation of matrix is started   %d\n",rank );
      calculate_matrices<double>(rank,number_of_processes);
      cout<<"are out  out of calculation\n";
    }
  }
// root process comes back
}

void wake_up_processes(int rank, int number_processes) { // only root can wake  up all processes

 const int TAG_WAIT = 14;

  if(rank == 0) {
    MPI_Request request;
    int send_val = 1, lowest_non_root_rank = 1;

    for( int i=lowest_non_root_rank; i< number_processes; i++) {
      MPI_Isend(&send_val, 1, MPI_INT, i, TAG_WAIT, MPI_COMM_WORLD, &request );
    }
      printf("wake_up_processes is sent\n");
   }
}


void wake_up_processes(int rank, int number_processes,int limit_number_process) { // only root can wake  up all processes

 const int TAG_WAIT = 14;

  if(rank == 0) {

    MPI_Request request;
    int send_val = 1, lowest_non_root_rank = 1;

    for( int i=lowest_non_root_rank; i< limit_number_process; i++) {
      MPI_Isend(&send_val, 1, MPI_INT, i, TAG_WAIT, MPI_COMM_WORLD, &request );
    }
      printf("wake_up_processes with limit is sent\n");
   }
}

void finalise_up_processes(int rank, int number_processes) { // only root can wake  up all processes

 const int TAG_WAIT = 14;
  if(rank == 0) {

    MPI_Request request;
    int send_val = TAG_WAIT, lowest_non_root_rank = 1;

    for( int i=lowest_non_root_rank; i < number_processes; i++) {
      MPI_Isend(&send_val, 1, MPI_INT, i, TAG_WAIT, MPI_COMM_WORLD, &request );
    }
      printf("finalize_up_processes is sent\n" );
   }
}


template <class T>
void calculate_matrices( int rank, int number_processes) {

    //   wake_up_processes(rank, number_processes);
   T *data_first = NULL, *data_second = NULL;
   MPI_Status status;
   MPI_Request request;

   static  int work_counter = 0;

   const int SIZE_PARAMETERS_ATTRIB = 20;
   int row_first, column_first, row_second, column_second;
   int low_bound=0, upper_bound=0; //low bound of the number of rows of [A] allocated to a slave
   int number_of_columns;


   cout<<"waiting is started\n";
      MPI_Irecv(&low_bound, 1, MPI_INT, 0, SIZE_PARAMETERS_ATTRIB, MPI_COMM_WORLD, &request);
      MPI_Irecv(&upper_bound, 1, MPI_INT, 0, SIZE_PARAMETERS_ATTRIB +1, MPI_COMM_WORLD, &request);

      row_first = upper_bound - low_bound;
      MPI_Irecv(&number_of_columns, 1, MPI_INT, 0, SIZE_PARAMETERS_ATTRIB + 2, MPI_COMM_WORLD, &request);

      column_first = number_of_columns;
      data_first = new(nothrow) T[(upper_bound - low_bound) * column_first];
      assert(data_first != NULL);


      printf( "number columns %d   rank  %d\n",number_of_columns,rank);

      assert( MPI_Irecv(data_first, (upper_bound - low_bound) * column_first, MPI_DOUBLE, 0,
              SIZE_PARAMETERS_ATTRIB + 3, MPI_COMM_WORLD, &request) == MPI_SUCCESS);



      MPI_Irecv(&column_second, 1, MPI_INT, 0, SIZE_PARAMETERS_ATTRIB + 4, MPI_COMM_WORLD, &request);
      MPI_Irecv(&row_second, 1, MPI_INT, 0, SIZE_PARAMETERS_ATTRIB + 5, MPI_COMM_WORLD, &request);

//      printf("all data received %d  number %d \n",rank,(upper_bound - low_bound) * column_first );

//      MPI_Wait(&request,&status);

      data_second = new T[row_second*column_second]();
      assert(data_second != NULL);

//      if ( work_counter == 1) {
//          MPI_Finalize();
//          exit(0);
//      }

      cerr<<"BCAST  CHILDRENNNNNNNNNNNNNNNNNNN"<<work_counter<<"\n";
      const int ROOT_PROCESS_ID = 0;
      assert(  MPI_Bcast( data_second, row_second * column_second,
                 MPI_DOUBLE, ROOT_PROCESS_ID, MPI_COMM_WORLD) == MPI_SUCCESS );


   T *data_result = new(nothrow) T[max(row_second,row_first)]();
   assert(data_result != NULL);


     int i = rank-1;
      for(int i =0; i< row_first; i++) {
          for( int j=0; j < row_second; j++ ) {
              for( int k=0; k< column_second; k++) {

                  data_result[j ] += data_first[k+i*column_first] * data_second[k + j *column_second ];

              }

          }



//          cout<<"\n\n";
//          assert( MPI_Finalize()== MPI_SUCCESS );
//          exit(0);


//         for(int j=0; j<column_first; j++) {
//             printf("%f ",data_first[j+i*column_first]);

//         for(int j=0; j<column_second; j++) {
        }



//      printf("RES %d\n",rank);
//      for(int i=0; i< row_second; i++) {
//          printf("%f ",data_result[i]);
//      }

//       printf("\n");

//   exit(0);
//       {
//           printf("Result  %d\n",rank);

//           for(int i=0; i <(upper_bound - low_bound) * column_second; i++) {
//               printf("%f ",data_result[i]);
//           }

//           printf("\n");
//        }


//           printf(" process is out %d\n", rank);
//              assert( MPI_Finalize()== MPI_SUCCESS );
//              exit(0);


//   MPI_Recv(array+i*size_columns_, this->size_rows_*second_operand.size_columns_,MPI_DOUBLE,i,RECV_RESULT+i,MPI_COMM_WORLD,&status);

   const int RECV_RESULT = 36;
   MPI_Send(data_result,row_second,MPI_DOUBLE,ROOT_PROCESS_ID,RECV_RESULT+rank-1,MPI_COMM_WORLD);


   delete []data_second;
   delete []data_first;
   delete []data_result;

   work_counter++;
}



#endif // A_PARRALLEL_H
