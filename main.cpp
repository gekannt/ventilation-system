#include "inclusion.h"
#include "matrix.h"
#include "euler.h"
#include "adams.h"
#include "a_parrallel.h"

// if matrice has 3 rows and 3 columns then
// 0,0 - indexes of the first element and
// 2,2 - indexes of the last element

int main(int argc, char *argv[]) {

  double input_Ax[]= { 1,	-1,	0,	-1,
                         0,	1,	-1,	0,
                         0,	0,	1,	0,
                         0,	0,	0,	1 };

  double input_Ay[] = {  0,	0,	-1,	0,	-1,	0,
                         0,	0,	0,	-1,	0,	0,
                        -1,	-1,	0,	0,	0,	0,
                         0,	0,	1,	0,	0,	-1  };

  double  input_H[] = { 4000,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0};


  double input_R[] = { // 10 * 10
                 1.3, 0, 0, 0, 0,    0, 0, 0, 0, 0,
                 0, 3.53, 0, 0, 0,   0, 0, 0, 0, 0,
                 0, 0, 1.34, 0, 0,   0, 0, 0, 0, 0,
                 0, 0, 0, 1.677, 0,  0, 0, 0, 0, 0,
                 0, 0, 0, 0, 1.98,   0, 0, 0, 0, 0,
                 0, 0, 0, 0, 0,      2.12, 0, 0, 0, 0,
                 0, 0, 0, 0, 0,      0, 2.17, 0, 0, 0,
                 0, 0, 0, 0, 0,      0, 0, 2.15, 0, 0,
                 0, 0, 0, 0, 0,      0, 0, 0, 2.15, 0,
                 0, 0, 0, 0, 0,      0, 0, 0, 0, 1.37 };


  // 6 *10
  double input_S[]=  {
  1,	1,	1,	0,	1,	0,	0,	0,	0,	0,
  1,	1,	1,	0,	0,	1,	0,	0,	0,	0,
  1,	0,	0,	0,	0,	0,	0,	0,	1,	0,
  1,	0,	0,	1,	0,	0,	0,	0,	0,	1,
  0,	0,	0,	-1,	0,	0,	1,	0,	0,	0,
  1,	1,	0,	0,	0,	0,	0,	1,	0,	0 };


  double input_Sy[] = {
                 1,	0,	0,	0,	0,	0,
                 0,	1,	0,	0,	0,	0,
                 0,	0,	0,	0,	1,	0,
                 0,	0,	0,	0,	0,	1,
                 0,	0,	1,	0,	0,	0,
                 0,	0,	0,	1,	0,	0
               };

  double input_Ky[] = {
      76,	0,	0,	0,	0,	0,
      0,	74,	0,	0,	0,	0,
      0,	0,	63,	0,	0,	0,
      0,	0,	0,	85,	0,	0,
      0,	0,	0,	0,	92,	0,
      0,	0,	0,	0,	0,	95
  };


  double input_Sx[] = {
      1,	1,	1,	0,
      1,	1,	1,	0,
      1,	0,	0,	0,
      1,	0,	0,	1,
      0,	0,	0,	-1,
      1,	1,	0,	0 };


  double input_Kx[]= {
  130,	0,	0,	0,
  0,	55,	0,	0,
  0,	0,	60,	0,
  0,	0,	0,	61 };

// Ax, Ay, H, R, S, Sy, Sx, Kx, Ky
  Matrix<double> Ax(input_Ax,4,4);
  Matrix<double> Ay(input_Ay,4,6);
  Matrix<double> H ( input_H,10,1);
  Matrix<double> R (input_R,10,10);
  Matrix<double> S(input_S,6,10);
  Matrix<double> Sy(input_Sy,6,6);
  Matrix<double> Sx(input_Sx,6,4);
  Matrix<double> Ky(input_Ky,6,6);
  Matrix<double> Kx(input_Kx,4,4);

 int rank=0, number_processes=0;

 assert( MPI_Init(&argc, &argv) ==  MPI_SUCCESS );   //initialize MPI operations
 assert(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == MPI_SUCCESS); //get the rank
 assert ( MPI_Comm_size(MPI_COMM_WORLD, &number_processes) == MPI_SUCCESS ); //get number of processes
 stall_non_root_processes(rank,number_processes);

 if(rank !=0 ) {
     cout<<"alarm1";
     exit(0);
 }



 Matrix <double> Ax_inversed;
 Ax_inversed = Ax.gauss_jordan_inverse_matrice();
 Ax_inversed.show("ax inv");
 Matrix <double> W = Ax_inversed * Ay;

 W.show("W");


 cout<<"parent finished";



//// if ( rank == 0) {
//     Ax_inversed.show("Ax inv");0
//     Ay.show("Ay");00

//     std::cout<<endl;
//     W.show("w");
//// }


 Matrix <double> TP = Sy * Ky;
 TP.show("TP");


 finalise_up_processes(rank,number_processes);
 MPI_Finalize();
 return 0;

//  Matrix <double> TP = (Sy*Ky- Sx*Kx*W).gauss_jordan_inverse_matrice() * S;
// Matrix <double> TP = Sy*Ky;  //- Sx*Kx*W).gauss_jordan_inverse_matrice() * S;

//  if ( rank == 0 ) {

//      TP.show("TP");
//  }





//  Euler <double>euler(Ax.get_size_columns(), Ay.get_size_columns(), TP, H, R, W);

//  Matrix<double> res = euler.calculate_fully();


  MPI_Finalize();
  return 0;


//  Adams <double>adam(Ax.get_size_columns(), Ay.get_size_columns(), TP, H, R, W);
//  adam.calculate();

}

