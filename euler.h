#ifndef EULER_H
#define EULER_H

#include "matrix.h"

template <class T>
class Adams;

template <class T>
class Euler {

 friend class Adams<T>;
 public:
    Euler<T>(int x_edges, int y_edges,  Matrix<T> &TP, Matrix<T> &H, Matrix<T> &R, Matrix<T> &W);
    ~Euler ();
    Matrix<T> calculate_fully();
    Matrix<T> calculate_one_step(Matrix <T> *X, Matrix<T> *Y);

 private:
  void set_initial_state();

  Matrix <T> *X_, *Y_;
  Matrix <T> &TP_,  &H_, &R_, &W_;
  const  double EPS = 0.001,//0.0015,
                      STEP_ = 0.1,
                      INITIAL_VALUE_ = 0.001;

};

template <class T>
void Euler<T>::set_initial_state() {
    X_->set_to_value(INITIAL_VALUE_);
    Y_->set_to_value(INITIAL_VALUE_);
    X_->transpose();
    Y_->transpose();
}

template <class T>
Matrix<T> Euler<T>::calculate_one_step(Matrix<T> *X, Matrix<T> *Y) {

    int rank=0; //process rank
    int number_processes=0; //number of processes

    assert(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == MPI_SUCCESS); //get the rank
    assert ( MPI_Comm_size(MPI_COMM_WORLD, &number_processes) == MPI_SUCCESS ); //get number of processes

    Matrix<T> Z, Yold, Xold,Zold;

    //    set_initial_state();
    X_->transpose();
    Y_->transpose();

     X_->show("x");
     Y_->show("y");

  Z= concatenate_vertically(*X_,*Y_);
  Xold = *X_;
  Yold = *Y_;

  double num=0.1;
  int number_iter=0;


//  while ( num > ( EPS*EPS) )  {
    x_mul_fabsx(Z);

    //1st block
    X_->transpose();
    Y_->transpose();
    Z.transpose();


    // 1st block

    //2nd
    *Y_ = *Y_ +   TP_ *  STEP_  *     (H_ -  R_*Z)   ;
    //3rd
//    Xold.transpose();
//    Yold.transpose();
    //3rd

    // 4th
    Zold = concatenate_vertically(Xold, Yold);



//    *Y_ =   *Y_ +  STEP_*1/2 *(3 * TP_ *   (H_ - R_ * Z)) -
//                               TP_ * (H_ - R_ * Zold);



    // 2nd  // trash , doesn't matter, goes from Y_
     *X_ =  W_ * *Y_ * -1;


   // 4th

   // 5th
     X_->transpose();
     Y_->transpose();
  // 5th

   //6th
     Z = concatenate_vertically(*X_, *Y_);
    // 6th

     num = sqr_difference(Z,Zold);
     cout<<"num  "<<num<<  "max error "<<EPS*EPS<<"\n";
     number_iter++;
//   }

  cout<<"number iter  "<<number_iter<<"\n";

  Xold = *X_;
  Yold = *Y_;


  *Y = *Y_;
  *X = *X_;



//  set_initial_state();
  X_->transpose();
  Y_->transpose();


  Z.show("ZZ");
  return Z;

}


template <class T>
Matrix<T> Euler<T>::calculate_fully() {

    int rank=0; //process rank
    int number_processes=0; //number of processes

    assert(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == MPI_SUCCESS); //get the rank
    assert ( MPI_Comm_size(MPI_COMM_WORLD, &number_processes) == MPI_SUCCESS ); //get number of processes

    set_initial_state();
    Matrix<T> Z = concatenate_vertically(*X_,*Y_), Yold, Xold,Zold;

    double num=0.1;
    int number_iter=0;

    //if (rank ==0)
  {

 while ( num > ( EPS*EPS) )  {

//     x_mul_fabsx(Z);
  if( rank ==0) {

   x_mul_fabsx(Z);
//
   //1st block
   X_->transpose();
   Y_->transpose();
   Z.transpose();


   Yold = *Y_;
   Xold = *X_;
   // 1st block
  }
   //2nd


//    Matrix <double> buf1 =  TP_ *  STEP_;
//    Matrix <double> buf2 =   R_ *Z;

  if (rank != 0)
  {
      Z.show("Z");
      R_.show("R");
  }

  if(rank !=0)
   Z.transpose();


   *Y_ = *Y_ +    TP_ *  STEP_  *      (H_ -  R_*Z);
//  Matrix<double> another =  R_*Z;


//  if(rank == 0)
//      W_ = *Y_;
   // 2nd
//  if (rank == 0)
//    Y_->show("Y_mod");

  if (rank == 1) {
      W_.show("W_1");
      Y_->show("Y_1");

  }

  if ( rank != 0 ) {
//    X_->transpose();
      W_.show("W_0");
      Y_->show("Y_0");

  }


   *X_ =  W_ * *Y_ * -1;

  MPI_Finalize();
  exit(0);


  if (rank ==0) {
   //3rd
   Xold.transpose();
   Yold.transpose();
   //3rd

   // 4th

   Zold = concatenate_vertically(Xold, Yold);

  // 4th

  // 5th
    X_->transpose();
    Y_->transpose();
 // 5th

  //6th

    Z = concatenate_vertically(*X_, *Y_);

   // 6th

    num = sqr_difference(Z,Zold);
//    cout<<"num  "<<num<<  "max error "<<EPS*EPS<<"\n";
    number_iter++;
    printf("number of iter %d \n",number_iter);

   }

  }

 printf("we are out %d \n",rank);

// cout<<"number iter  "<<number_iter<<"\n";

 set_initial_state();
}


 if ( rank == 0 ) {
//     for( ; ;)
//     printf(" I'm done\n");
  return Z;
 }


}


#include "euler_tpp.h"

#endif // EULER_H
