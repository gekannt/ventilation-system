#ifndef MATRIX_INVERSING_H
#define MATRIX_INVERSING_H


template <class T>
Matrix<T> Matrix<T>::gauss_jordan_inverse_matrice() {
//  show("before");

  Matrix<T> identity_matrix(size_rows_, size_columns_);
  identity_matrix.make_indentity_matrix();

//  int rank;
//  assert(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == MPI_SUCCESS); //get the rank

//  if( rank != 0) {
//      return identity_matrix;
//  }

//  identity_matrix.show("matrix identity");
//  cout<<endl;

//  return  identity_matrix;

  // direct step down
  for(int current_row=0; current_row< size_rows_; current_row++) {  // won't it iliminate us to only
    remove_possible_zero_from_row_in_the_first_column(current_row,current_row,identity_matrix);
//    show("removed zero");

    make_row_equal_to_one(current_row,current_row,identity_matrix);
//    show("after make_row_equal_to_one");
    make_lower_elements_equal_to_zero(current_row,current_row+1,current_row,identity_matrix);
//    show("after make_lower_elements_equal_to_zero");
  }

//  show("main matrice");
//  identity_matrix.show("before back step");
//  return identity_matrix;

  // back step up
  int shift = size_rows_-1;
  for(int  i= size_rows_-1; i > 0; i--)  {
    make_lower_elements_equal_to_zero_back(i,i-1,shift,identity_matrix);
    shift--;
  }
 return  identity_matrix;
}

template <class T>
int  Matrix<T>::remove_possible_zero_from_row_in_the_first_column(int number_of_row, int offset, Matrix<T> &identity_matrice) {

    if ( data_[number_of_row*size_columns_ + offset] != 0 ) return true;// already not zero

    int i;
    for(i=number_of_row+1; i<size_rows_; i++) {
        if ( data_[i*size_columns_ + offset] != 0) {
            for(int k=0; k<size_columns_;k++) {
                swap(data_[number_of_row*size_columns_+k],data_[i*size_columns_ + k]);
                swap(identity_matrice.data_[number_of_row*size_columns_+k],identity_matrice.data_[i*size_columns_ + k]);
            }
            return true;
        }
    }

   // if ( i == size_rows_) // we are here , because this condition happened
   error_warning("failed with deleting of leading zero");
   return false;
}

template <class T>
void Matrix<T>::swap_rows(int first, int second) {

    if ( first >= size_rows_ || second >= size_rows_)
        error_fatal("bounds of matrice are broken");

    for(int i=0; i< size_columns_; i++) {
        swap (data_[first*size_columns_ +i ], data_[second*size_columns_+ i] );
    }
}


template <class T>
void Matrix<T>::make_row_equal_to_one(int number_of_row,int offset, Matrix<T> &identity_matrice) {
    double val = data_[number_of_row*size_columns_+offset];
    val = 1 / val;
    for ( int i =0; i < size_columns_; i++ ) {
        data_[number_of_row * size_columns_ + i]  *= val;
        identity_matrice.data_[number_of_row * size_columns_ + i]  *= val;
    }
}


template <class T>
void Matrix<T>::make_lower_elements_equal_to_zero(int number_of_row_source_of_action, int number_of_row_to_do_action,
                                                  int offset, Matrix<T> &identity_matrice) {

    for ( int i = number_of_row_to_do_action; i < size_rows_; i++) {
         T first_elem = data_[i*size_columns_+offset]*-1;
        for(int j=0; j < size_columns_; j++) {
            data_[i*size_columns_+j] += ( data_[number_of_row_source_of_action*size_columns_ +j ] * first_elem);
            identity_matrice.data_[i*size_columns_+j] += ( identity_matrice.data_[number_of_row_source_of_action*size_columns_ +j ] * first_elem);
        }
    }
}

template <class T>
void Matrix<T>::make_lower_elements_equal_to_zero_back(int number_of_row_source_of_action, int number_of_row_to_do_action,
                                                       int offset,  Matrix<T> &identity_matrice) {

    for ( int i = number_of_row_to_do_action; i > -1; i--) {
         T first_elem = data_[i*size_columns_+offset]*-1;

        for(int j=size_columns_; j > 0; j--) {
            data_[i*size_columns_ + j-1] +=  ( data_[number_of_row_source_of_action*size_columns_ +j -1] * first_elem ) ;
            identity_matrice.data_[i*size_columns_+j -1] +=
                     ( identity_matrice.data_[number_of_row_source_of_action*size_columns_ +j -1] * first_elem );
        }
//        show("matrix_step_done usual");
//        identity_matrice.show("step done identity");
//        cout<<endl<<endl;
    }
}




#endif // MATRIX_INVERSING_H
