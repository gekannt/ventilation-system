#ifndef EULER_TPP_H
#define EULER_TPP_H


template <class T>
Euler<T>::Euler(int x_edges, int y_edges, Matrix<T> &TP, Matrix<T> &H,  Matrix<T> &R, Matrix<T> &W):
    TP_(TP), H_(H), R_(R), W_(W)  {

    X_ = new(nothrow) Matrix<T> (x_edges,1);
    Y_ = new(nothrow) Matrix<T> (y_edges,1);
//    Z_block_ = NULL;

    if ( !X_  || !Y_)
      ERROR_EXIT("can't allocate space");
}

template <class T>
void x_mul_fabsx(Matrix<T> &first) {

    int number_of_elements= first.get_size_columns() * first.get_size_rows();
    T *data_first = first.get_pointer_to_data();

  for(int i=0;i<number_of_elements ;i++)
      data_first[i] = data_first[i] * fabs(data_first[i]);
}


template <class T>
double sqr_difference(const Matrix<T> &first, const Matrix<T> &second) {
    double sum=0;
    if ( first.get_size_columns() != second.get_size_columns() ||  first.get_size_rows() != second.get_size_rows() ) {
        cout<<"first  "<< first.get_size_rows() <<"   " <<first.get_size_columns()<<"\n";
        cout<<"first  "<< second.get_size_rows() <<"   " <<second.get_size_columns()<<"\n";
        ERROR_EXIT("norm can't be calculated, not equal size of matrices");
     }

    int number_of_elements= first.get_size_columns() * first.get_size_rows();
    T *data_first = first.get_pointer_to_data(), *data_second = second.get_pointer_to_data();

    double buf;
  for(int i=0;i<number_of_elements ;i++) {
//      sum += ( data_first[i] - data_second[i] ) *  ( data_first[i] - data_second[i] ) );
     buf = data_first[i] - data_second[i];
     buf = buf*buf;
     sum += buf;
  }
  return sum;
}



template <class T>
Euler<T>::~Euler() {
    if ( X_ != NULL) {
        delete X_;
        X_=NULL;
    }
    if ( Y_ != NULL) {
        delete Y_;
        Y_=NULL;
    }
}


#endif // EULER_TPP_H
