#ifndef ADAMS_H
#define ADAMS_H

#include "euler.h"


template <class T>
class Adams
{
 public:
   explicit  Adams<T>(int x_edges, int y_edges,  Matrix<T> &TP, Matrix<T> &H, Matrix<T> &R, Matrix<T> &W);
    ~Adams();
   void calculate();
 private:
   Euler<T>  *euler_;
   Matrix<T> predictor(Matrix<T> &yk_old, Matrix<T> &z0, Matrix<T> &z1,Matrix<T> &z2,Matrix<T> &z3);
   Matrix<T> correct(Matrix<T> &yk_old, Matrix<T> &z0, Matrix<T> &z1,Matrix<T> &z2,Matrix<T> &z3);

  // strange constants, let it be
   const static int PREDICTOR_K3_=55,PREDICTOR_K2_=-59,
                PREDICTOR_K1_=37,PREDICTOR_K0_=-9, DIVISOR_=24;

   const  double EPS = 0.001,//0.0015,
                       STEP_ = 0.1,
                       INITIAL_VALUE_ = 0.001;


};

template <class T>
Adams<T>::Adams(int x_edges, int y_edges,  Matrix<T> &TP, Matrix<T> &H, Matrix<T> &R, Matrix<T> &W) {
 euler_ = new Euler<T>(x_edges,y_edges,TP, H, R, W);
}

template <class T>
Adams<T>::~Adams() {
  delete  euler_;
}

template <class T>
Matrix<T> Adams<T>::predictor(Matrix<T> &yk_old, Matrix<T> &z0, Matrix<T> &z1,Matrix<T> &z2,Matrix<T> &z3) {

    Matrix <T> pk = yk_old;
//    z3 = z3*K3_;
//    z2 = z2*K2_;
//    z1 = z1*K1_;
//    z0 = z0*K0_;
     pk=yk_old + ( z0*PREDICTOR_K0_  + z1*PREDICTOR_K1_  + z2*PREDICTOR_K2_ + z3*PREDICTOR_K3_ ) *    (euler_->STEP_/DIVISOR_ );
    return pk;
}

template <class T>
Matrix<T> Adams<T>::correct(Matrix<T> &yk_old, Matrix<T> &z0, Matrix<T> &z1,Matrix<T> &z2,Matrix<T> &z3) {

    Matrix<T> yk_p_1 = yk_old;


//    (z1 - z2*5+ z3*19  + 9  );

}

//*euler_->Y_ =   *euler_->Y_ + ( euler_->STEP_*0.5 * ( (3 * euler_->TP_ *         (euler_->H_ - euler_->R_ * Z)) )
//        - ( euler_->TP_ * (euler_->H_ - euler_->R_ * Z)));



template <class T>
void Adams<T>::calculate() {

 Matrix<T> *X_= euler_->X_;
 Matrix<T> *Y_= euler_->Y_;
 Matrix<T> &TP_ = euler_->TP_;
 Matrix<T> &H_= euler_->H_;
 Matrix<T> &R_= euler_->R_;
 Matrix<T> &W_= euler_->W_;

Matrix<T> Yold, Xold;
Xold = *X_;
Yold = *Y_;
Yold.set_to_value(INITIAL_VALUE_);
Xold.set_to_value(INITIAL_VALUE_);



Matrix<T> Zold = euler_->calculate_one_step(&Xold,&Yold);
Matrix <T> Z = euler_->calculate_one_step(X_,Y_);




 X_->transpose();
 Y_->transpose();


     X_->show("x");
     Y_->show("y");





//   Xold.transpose();
//   Yold.transpose();
//  Zold = concatenate_vertically(Xold,Yold);




  Z.show("Z");
  Zold.show("Zold");
  Xold.show("xold");
  Yold.show("yold");
  X_->show("x_");
  Y_->show("y_");
//  exit(0);
//  Xold.set_to_value(INITIAL_VALUE_);
//  Yold.set_to_value(INITIAL_VALUE_);


  double num=0.1;
  int number_iter=0;

  while ( num > ( EPS*EPS) )  {
    x_mul_fabsx(Z);

    //1st block
    X_->transpose();
    Y_->transpose();
    Z.transpose();

    Yold = *Y_;
    Xold = *X_;
    // 1st block


    Zold.transpose();

    //2nd
//    *Y_ = *Y_ +      TP_ *  STEP_  *      (H_ -  R_*Z);

    *Y_ = *Y_ +   STEP_ *(  3 * TP_  *
            (H_ -  R_*Z)   -   TP_* (H_ - R_ * Zold )   ) *2;

    //*euler_->Y_ =   *euler_->Y_ + ( euler_->STEP_*0.5 * ( (3 * euler_->TP_ *         (euler_->H_ - euler_->R_ * Z)) )
    //        - ( euler_->TP_ * (euler_->H_ - euler_->R_ * Z)));


    Zold.transpose();
    // 2nd
     *X_ =  W_ *  *Y_ *  -1;

    //3rd
    Xold.transpose();
    Yold.transpose();
    //3rd

    // 4th
    Zold = concatenate_vertically(Xold, Yold);
   // 4th

   // 5th
     X_->transpose();
     Y_->transpose();
  // 5th

   //6th

     // разобрать назад Z
     Z = concatenate_vertically(*X_, *Y_);
    // 6th
//     Zold.show("Zold");
//     Z.show("Z");
//     exit(0);

     num = sqr_difference(Z,Zold);
     cout<<"num  "<<num<<  "max error "<<EPS*EPS<<"\n";
     number_iter++;

   }

  cout<<"number iter  "<<number_iter<<"\n";

//  set_initial_state();
  Zold.show("Zold");
  Z.show("Z");
}

#endif // ADAMS_H
